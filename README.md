# Panunda

<!-- =============================================================================================================== -->
<!-- ===== Prerequis =========================================================================================== -->
<!-- =============================================================================================================== -->

## Presentation

Panunda est un système de pilotage pour les services et régies de l'eau et de l'assainissement bati autour d'une solution d'Informatique décisionelle (Pentaho Community Edition).
Il permet de suivre les indicateurs cardinaux dans l'ensemble des domaines de l'exploitation.

## Principe
Les données sont extraites des différentes applications par l'ETL Pentaho Data Integration puis stockées dans un entrepot de données.
Elles sont alors rendues accessibles pour leur publication dans des indicateurs regroupés en tableaux de bord ou par des interrogations "ad hoc".

## Type de licence
GPLV2

## Contenu de la distribution 
Cette disibution contient deux tableaux de bord : 
- Pan_Eau_Ad pour l'eau potable
- Pan_Asst_Ad pour l'assainissement

## Prerequis: Pentaho 7 CE

https://community.hds.com/docs/DOC-1009931-downloads

## Installation

a) dans Pentaho creer une connexion avec la base qui contient vos données

b) charger dans PCU les contenus des deux dashboards Pan_Eau_Ad et Pan_Asst_Ad
